# QuizNight dokumentáció

## Telepítés
1. Legacy db felállítása
2. .env fájl kitöltése
3. config/database.php-ben a "legacy" kapcsolat adatainak kitöltése
```
composer install
npm install
php artisan migrate
php artisan migrate:legacy
gulp
```

Mix + Gulp használatához segítség: https://laravel.com/docs/master/mix

## Modellek

### Badge

Relationships:
| Name | Model | Type |
|--- | --- | --- |
| badgeCategory | BadgeCategory | belongsTo|
| teams | Team | belongsToMany |

Attributes:
- Image

### BadgeCategory

Relationships:
| Name | Model | Type |
|--- | --- | --- |
|badges | Badge | HasMany|

### Championship

### Event
Relationships:
| Name | Model | Type |
|--- | --- | --- |
|results | EventResult | hasMany|
|championship | Championship | belongsTo|

### EventResult
Relationships:
| Name | Model | Type |
|--- | --- | --- |
|event|Event|belongsTo|
|venue|Venue|belongsTo|
|winner|Team|belongsTo|
|lightningWinner|Team|belongsTo|

### Image
Non-persistent model

Több info: config/images.php, app/Models/Image.php

### OfferRequest

### Page

### Permission

Relationships:
| Name | Model | Type |
|--- | --- | --- |
|users|User|belongsToMany|

### Post
Relationships:
| Name | Model | Type |
|--- | --- | --- |
|author|User|belongsTo|

### Question
Relationships:
| Name | Model | Type |
|--- | --- | --- |
|user|User|belongsTo|

### Reservation
Relationships:
| Name | Model | Type |
|--- | --- | --- |
|event|Event|belongsTo|
|venue|Venue|belongsTo|
|user|User|belongsTo|
|team|Team|belongsTo|

### Score
Relationships:
| Name | Model | Type |
|--- | --- | --- |
|event|Event|belongsTo|
|venue|Venue|belongsTo|
|admin|User|belongsTo|
|team|Team|belongsTo|

### Team
Relationships:
| Name | Model | Type |
|--- | --- | --- |
|users|User|belongsToMany|
|badges|Badge|belongsToMany|

Attributes:
- Image

### User
Relationships:
| Name | Model | Type |
|--- | --- | --- |
|permissions|Permission|belongsToMany|
|teams|Team|belongsToMany|
|questions|Question|hasMany|
|posts|Post|hasMany|

### Venue
Relationships:
| Name | Model | Type |
|--- | --- | --- |
|tags|VenueTag|belongsToMany|

Attributes:
- Image

### VenueTag
Relationships:
| Name | Model | Type |
|--- | --- | --- |
|venues|Venue|belongsToMany|
