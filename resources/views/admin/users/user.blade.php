@extends('admin.layout')

@section('title')
    {{ $user->name }} felhasználó adatai
@endsection


@section('content')
    <div class="container is-fluid">
        <div class="columns">
            <div class="column is-7">
                <div class="box">
                    <p class="subtitle">Törzsadatok</p>
                    <form method="post" action="{{ route('admin.user.update', $user) }}">
                        {{ csrf_field() }}
                        <label for="name" class="label">Teljes név</label>
                        <p class="control">
                            <input type="text" name="name" id="name" class="input{{ $errors->has('name') ? ' is-danger' : '' }}" value="{{ old('name') ?? $user->name }}">
                            @if($errors->has('name'))
                                <span class="help is-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </p>
                        <label for="phone" class="label">Telefonszám</label>
                        <p class="control">
                            <input type="text" name="phone" id="phone" class="input" value="{{ old('phone') ?? $user->phone }}">
                        </p>
                        <label for="name" class="label">Felhasználó név</label>
                        <p class="control">
                            <input type="text" name="username" id="username" class="input{{ $errors->has('username') ? ' is-danger' : '' }}" value="{{ old('username') ?? $user->username }}">
                            @if($errors->has('username'))
                                <span class="help is-danger">{{ $errors->first('username') }}</span>
                            @endif
                        </p>
                        <label for="name" class="label">E-mail</label>
                        <p class="control">
                            <input type="email" name="email" id="email" class="input{{ $errors->has('email') ? ' is-danger' : '' }}" value="{{ old('email') ?? $user->email }}">
                            @if($errors->has('email'))
                                <span class="help is-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </p>
                        <label for="gender" class="label">Nem</label>
                        <p class="control">
                            <span class="select">
                                <select name="gender" id="gender">
                                    <option value="male" {{ old('gender') == 'male' || $user->gender == 'male' ? 'selected' : '' }}>Férfi</option>
                                    <option value="female" {{ old('gender') == 'male' || $user->gender == 'female' ? 'selected' : '' }}>Nő</option>
                                </select>
                            </span>
                        </p>
                        <label for="description" class="label">Leírás</label>
                        <p class="control">
                            <textarea name="description" id="description" class="textarea">{{ $user->description }}</textarea>
                        </p>

                        <button class="button is-primary">
                            <span class="icon"><i class="fa fa-save"></i></span>
                            <span>Mentés</span>
                        </button>
                    </form>
                </div>
            </div>
            <div class="column is-5">
                <div class="box">
                    <p class="subtitle">Profilkép</p>
                    <img src="{{ $user->avatar->small()->getUrl() }}">
                    <form method="post" action="{{ route('admin.user.avatar.store', $user) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="file" name="image">
                        <button class="button is-primary">
                            <span class="icon"><fa class="fa-upload"></fa></span>
                            <span>Mentés</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>


    </div>
@endsection