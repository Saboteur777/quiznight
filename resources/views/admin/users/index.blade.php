@extends('admin.layout')

@section('title')
    Felhasználók
@endsection

@section('content')
    <div class="container is-fluid">
        <form method="get" action="{{ route('admin.users.search') }}" >
            {{ csrf_field() }}
            <div class="control is-horizontal has-text-centered">
                <p class="control has-addons-right">
                    <input type="text" class="input" name="q" placeholder="Név, felhaszánlónév, email">
                </p>
                <button class="button is-primary">
                    <span class="icon">
                        <i class="fa fa-search"></i>
                    </span>
                    <span>Keresés</span>
                </button>
            </div>
        </form>

        <table class="table is-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Kép</th>
                    <th>Név</th>
                    <th>E-mail</th>
                    <th>Csapatok</th>
                    <th>Műveletek</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td><img src="{{ $user->image->squareThumb()->getUrl() }}"></td>
                        <td><a href="{{ route('admin.user.get', $user) }}">{{ $user->name }}</a></td>
                        <td>{{ $user->email }}</td>
                        <td>{{-- *$user->teams --}}</td>
                        <td>
                            <a href="{{ route('admin.user.get', $user) }}" class="button is-success">
                                <span class="icon"><i class="fa fa-edit"></i></span>
                            </a>
                            <i class="button is-danger is-outlined">
                                <span class="icon"><fa class="fa fa-times"></fa></span>
                            </i>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
    </div>
@endsection