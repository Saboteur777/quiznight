@extends('admin.layout')

@section('content')
    <div class="container is-fluid">
        {{--<form method="get" action="{{ route('admin.users.search') }}" >
            {{ csrf_field() }}
            <div class="control is-horizontal has-text-centered">
                <p class="control has-addons-right">
                    <input type="text" class="input" name="q" placeholder="Név, felhaszánlónév, email">
                </p>
                <button class="button is-primary">
                    <span class="icon">
                        <i class="fa fa-search"></i>
                    </span>
                    <span>Keresés</span>
                </button>
            </div>
        </form>--}}

        <table class="table is-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Kép</th>
                <th>Név</th>
                <th>Kategória</th>
            </tr>
            </thead>
            <tbody>
            @foreach($badges as $badge)
                <tr>
                    <td>{{ $badge->id }}</td>
                    <td><img src="{{ $badge->image->squareSmall()->getUrl() }}"></td>
                    <td><a href="#">{{ $badge->name }}</a></td>
                    <td>{{ $badge->badgeCategory->display_name }}</td>
                    <td>{{-- *$user->teams --}}</td>
                    <td>
                        <a href="#" class="button is-success">
                            <span class="icon"><i class="fa fa-edit"></i></span>
                        </a>
                        <i class="button is-danger is-outlined">
                            <span class="icon"><fa class="fa fa-times"></fa></span>
                        </i>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $badges->links() }}
    </div>
@endsection