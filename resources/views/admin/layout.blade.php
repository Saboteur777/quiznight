<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin - Free Bulma template</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ mix('/css/admin.css') }}">
</head>
<body>
<div class="columns">
    <aside class="column is-2 aside hero is-fullheight is-hidden-mobile">
        <div>
            <div class="main">
                <div class="title">Navigáció</div>
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-home"></i></span><span class="name"> Dashboard</span></a>
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-calendar-o"></i></span><span class="name"> Események</span></a>
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-check-square-o"></i></span><span class="name"> Pontszámok</span></a>
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-question"></i></span><span class="name"> Kérdések</span></a>
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-suitcase"></i></span><span class="name"> Ajánlatkérések</span></a>
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-file"></i></span><span class="name"> Tartalmak</span></a>
                @can('handle', App\Models\User::class)
                    <a href="{{ route('admin.users.index') }}" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-user"></i></span><span class="name"> Felhasználók</span></a>
                @endcan
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-trophy"></i></span><span class="name"> Bajnokságok</span></a>
                @can('handle', App\Models\Team::class)
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-group"></i></span><span class="name"> Csapatok</span></a>
                @endcan
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-map-marker"></i></span><span class="name"> Helyszínek</span></a>
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-newspaper-o"></i></span><span class="name"> Hírek</span></a>
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-certificate"></i></span><span class="name"> Jelvények</span></a>
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-cutlery"></i></span><span class="name"> Asztalfoglalás</span></a>
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-sitemap"></i></span><span class="name"> Jelvény kategóriák</span></a>
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-user-times"></i></span><span class="name"> Tömeges jelvény aktiválás</span></a>
                <a href="#" class="item"><span class="icon is-hidden-tablet-only"><i class="fa fa-globe"></i></span><span class="name"> Aloldalak</span></a>
                <a href="{{ route('admin.logout') }}" class="item is-hidden-tablet-only" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                    <span class="icon"><i class="fa fa-sign-out"></i></span>
                    <span class="name"> Kijelentkezés</span>
                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </a>
            </div>
        </div>
    </aside>
    <div class="column is-10 admin-panel">
        <nav class="nav has-shadow" id="top">
            <div class="container">
                <div class="nav-left">
                    <a class="nav-item" href="../index.html">
                        <img src="../images/bulma.png" alt="QuizNight">
                    </a>
                </div>
          <span class="nav-toggle">
            <span></span>
            <span></span>
            <span></span>
          </span>
                <div class="nav-right nav-menu is-hidden-tablet">
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-home"></i></span><span class="name"> Dashboard</span></a>
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-calendar-o"></i></span><span class="name"> Események</span></a>
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-check-square-o"></i></span><span class="name"> Pontszámok</span></a>
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-question"></i></span><span class="name"> Kérdések</span></a>
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-suitcase"></i></span><span class="name"> Ajánlatkérések</span></a>
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-file"></i></span><span class="name"> Tartalmak</span></a>
                    @can('handle', App\Models\User::class)
                        <a href="{{ route('admin.users.index') }}" class="nav-item"><span class="icon"><i class="fa fa-user"></i></span><span class="name"> Felhasználók</span></a>
                    @endcan
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-trophy"></i></span><span class="name"> Bajnokságok</span></a>
                    @can('handle', App\Models\Team::class)
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-group"></i></span><span class="name"> Csapatok</span></a>
                    @endcan
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-map-marker"></i></span><span class="name"> Helyszínek</span></a>
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-newspaper-o"></i></span><span class="name"> Hírek</span></a>
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-certificate"></i></span><span class="name"> Jelvények</span></a>
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-cutlery"></i></span><span class="name"> Asztalfoglalás</span></a>
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-sitemap"></i></span><span class="name"> Jelvény kategóriák</span></a>
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-user-times"></i></span><span class="name"> Tömeges jelvény aktiválás</span></a>
                    <a href="#" class="nav-item"><span class="icon"><i class="fa fa-globe"></i></span><span class="name"> Aloldalak</span></a>
                    <a href="{{ route('admin.logout') }}" class="nav-item" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <span class="icon"><i class="fa fa-sign-out"></i></span>
                        <span class="name"> Kijelentkezés</span>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </a>
                </div>
            </div>
        </nav>
        @if(session()->has('status'))
            <div class="notification is-{{ session('status')['type'] }}">
                <button class="delete"></button>
                {{ session('status')['message'] }}
            </div>
        @endif
        @yield('content')
    </div>
</div>
<footer class="footer">
    <div class="container">
        <div class="has-text-centered">
            <p>
                <strong>QuizNight Admin felület</strong>
            </p>
        </div>
    </div>
</footer>
<script type="text/javascript" src="{{ mix('/js/admin.js') }}"></script>
</body>
</html>