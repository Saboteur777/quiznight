<?php

/**
 * Abstract seeder for config files. It takes the array items from the config files, and created instances of the given
 * Eloquent model.
 */
class ConfigSeeder
{
    /**
     * @var Items of config
     */
    protected $items;

    /**
     * @var Eloquent class
     */
    protected $class;

    /**
     * ConfigSeeder constructor.
     * @param $config
     * @param $class
     */
    public function __construct($config, $class)
    {
        $this->items = $config;
        $this->class = $class;
    }

    /**
     * Seed the config values
     */
    public function seed()
    {
        foreach ($this->items as $item) {
            // Check DB existence for every item to avoid duplicated permissions
            if (!call_user_func_array([$this->class, 'where'], ['name', $item['name']])->first()) {
                // Reguard model, this is important to only seed correct keys to the db
                call_user_func([$this->class, 'reguard']);
                // Seed item to db
                call_user_func_array([$this->class, 'create'], [$item]);
            }
        }
    }
}