<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new ConfigSeeder(config('permissions'), \App\Models\Permission::class))->seed();
    }
}
