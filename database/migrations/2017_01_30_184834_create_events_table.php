<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('championship_id')->unsigned()->nullable();
            $table->foreign('championship_id')->references('id')->on('championships');
            $table->string('name');
            $table->string('slug');
            $table->dateTime('date')->nullable();
            $table->text('description')->nullable();
            $table->boolean('championship');
            $table->boolean('is_private');
            $table->boolean('is_active');
            $table->dateTime('reservation_starts')->nullable();
            $table->dateTime('reservation_modify_ends')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
