<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('app.welcome');
});

Auth::routes();

Route::group(['middleware' => ['web', 'auth']], function() {
    Route::get('/home', 'HomeController@index');
});


// Admin routes

Route::group(['prefix' => 'admin'], function() {
    Route::get('register', 'Admin\Auth\RegisterController@showRegistrationForm');
    Route::post('register', 'Admin\Auth\RegisterController@register');
    Route::get('login', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Admin\Auth\LoginController@login')->name('admin.login');
    Route::post('logout', 'Admin\Auth\LoginController@logout')->name('admin.logout');

    // Admin auth routes
    Route::group(['middleware' => ['web', 'auth.admin']], function() {
        Route::get('dashboard', function() {
            return view('admin.dashboard');
        })->name('admin.dashboard');

        // User routes
        Route::group(['middleware'=> 'can:handle,App\Models\User'], function() {
            Route::get('/users', 'Admin\UserController@index')->name('admin.users.index');
            Route::get('users/search', 'Admin\UserController@search')->name('admin.users.search');
            Route::get('users/{user}', 'Admin\UserController@get')->name('admin.user.get');
            Route::post('users/update/{user}', 'Admin\UserController@update')->name('admin.user.update');
            Route::post('users/avatar/store/{user}', 'Admin\UserAvatarController@store')->name('admin.user.avatar.store');
        });

        // TODO badge policy
        // Badges
        Route::get('/badges', 'Admin\BadgeController@index')->name('admin.badges.index');
    });

});


