<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Permissions
      |--------------------------------------------------------------------------
      |
      | This array contains the definitions of the permissions that the
      | application uses. They are being written in the database by
      | the PermissionsTableSeeder.
      |
      */

    [
        'name' => 'superadmin',
        'display_name' => 'Szuperadmin'
    ],
    [
        'name' => 'users',
        'display_name' => 'Felhasználók',
        'model' => 'User'
    ],
    [
        'name' => 'teams',
        'display_name' => 'Csapatok',
        'model' => 'Team'
    ],
    [
        'name' => 'events',
        'display_name' => 'Események',
        'model' => 'Event'
    ],
    [
        'name' => 'scores',
        'display_name' => 'Pontszámok',
        'model' => 'Score'
    ],
    [
        'name' => 'questions',
        'display_name' => 'Kérdések',
        'model' => 'Question'
    ],
    [
        'name' => 'offer_requests',
        'display_name' => 'Ajánlatkérések',
        'model' => 'OfferRequest'
    ],
    [
        'name' => 'pages',
        'display_name' => 'Tartalmak',
        'model' => 'Page'
    ],
    [
        'name' => 'championships',
        'display_name' => 'Bajnokságok',
        'model' => 'Championship'
    ],
    [
        'name' => 'venues',
        'display_name' => 'Helszínek',
        'model' => 'Venue'
    ],
    [
        'name' => 'posts',
        'display_name' => 'Hírek',
        'model' => 'Post'
    ],
    [
        'name' => 'badges',
        'display_name' => 'Jelvények',
        'model' => 'Badge'
    ],
    [
        'name' => 'reservations',
        'display_name' => 'Asztalfoglalások',
        'model' => 'Reservation'
    ],
    [
        'name' => 'badge_vategories',
        'display_name' => 'Jelvény kategóriák',
        'model' => 'BadgeCategory'
    ]
];