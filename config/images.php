<?php

return [
    'squareThumb' => [
        'prefix' => 's_thmb_',
        'width' => 35,
        'height' => 35,
        'fixed_ratio' => true
    ],
    'squareSmall' => [
        'prefix' => 's_small_',
        'width' => 100,
        'height' => 100,
        'fixed_ratio' => true
    ],
    'thumbnail' => [
        'prefix' => 'thmb_',
        'width' => 100,
        'height' => 130,
        'fixed_ratio' => false
    ],
    'small' => [
        'prefix' => 'small_',
        'width' => 200,
        'height' => 260,
        'fixed_ratio' => false
    ],
    'large' => [
        'prefix' => 'large_',
        'width' => 500,
        'height' => 600,
        'fixed_ratio' => false
    ],
    'max' => [
        'prefix' => 'max_',
        'width' => 1024,
        'height' => 768,
        'fixed_ratio' => false
    ],

];