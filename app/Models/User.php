<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Image;

class User extends Authenticatable
{
    use Notifiable;

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getImageAttribute($value)
    {
        return new Image($value, 'users/');
    }

    #region Relationships

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }


    public function teams()
    {
        return $this->belongsToMany(Team::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    #endregion
}
