<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Image;

class Team extends Model
{
    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function badges()
    {
        return $this->belongsToMany(Badge::class);
    }

    public function getImageAttribute($value)
    {
        return new Image($value, 'teams/');
    }
}
