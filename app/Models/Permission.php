<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = ['name', 'display_name'];

    #region Relationships

    public function users(){
        return $this->belongsToMany(User::class);
    }

    #endregion
}
