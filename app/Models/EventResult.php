<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventResult extends Model
{
    protected $guarded = [];
    
    public function event(){
        return $this->belongsTo(Event::class);
    }

    public function venue(){
        return $this->belongsTo(Venue::class);
    }

    public function winner(){
        return $this->belongsTo(Team::class);
    }

    public function lightningWinner(){
        return $this->belongsTo(Team::class);
    }
}
