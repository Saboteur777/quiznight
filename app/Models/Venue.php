<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Image;

class Venue extends Model
{
    protected $guarded = [];

    public function getImageAttribute($value)
    {
        return new Image($value, 'venues/');
    }

    public function tags()
    {
        return $this->belongsToMany(VenueTag::class);
    }
}
