<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    protected $guarded = [];

    public function badgeCategory()
    {
        return $this->belongsTo(BadgeCategory::class);
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class);
    }

    public function getImageAttribute($value)
    {
        return new Image($value, 'badges/');
    }

    public function scopeActive($query){
        return $query->where('is_active', 1);
    }
}
