<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferRequest extends Model
{
    protected $guarded = [];
}
