<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = [];

    protected $dates = [
        'date',
        'reservations_starts',
        'reservation_modify_ends',
    ];

    public function results()
    {
        return $this->hasMany(EventResult::class);
    }

    public function championship()
    {
        return $this->belongsTo(Championship::class);
    }
}
