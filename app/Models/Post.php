<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // TODO nullázni az authort, venuet törlésnél
    protected $guarded = [];

    public function author()
    {
        return $this->belongsTo(User::class);
    }
}
