<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VenueTag extends Model
{
    protected $guarded = [];

    public function venues()
    {
        return $this->belongsToMany(Venue::class);
    }
}
