<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;

class Image
{
    protected $filename, $storageDir, $variant;

    public function __construct($filename, $storageDir)
    {
        $this->filename = $filename;
        $this->storageDir = $storageDir;
    }

    public function getUrl()
    {
        return Storage::url($this->storageDir.$this->variant);
    }

    function __call($func, $params)
    {
        if(array_key_exists($func, config('images'))) {
            $this->variant = config('images')[$func]['prefix'].$this->filename;
            return $this;
        }
        return parent::__call($func, $params);
    }
}