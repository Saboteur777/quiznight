<?php

namespace App\Console\Commands;

use App\Models\Event;
use App\Models\Reservation;
use App\Models\Team;
use App\Models\User;
use App\Models\Venue;
use DB;
use Illuminate\Console\Command;

class MigrateReservations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:reservations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function() {
            $this->info('Migrating reservations');
            $reservations = DB::connection('legacy')->table('reservation')->get();
            $bar = $this->output->createProgressBar(count($reservations));

            $reservations->each(function($reservation) use ($bar) {
                $oldEvent = DB::connection('legacy')->table('event')->where('id', $reservation->event_id)->first();
                $event = $oldEvent ? Event::where('slug', $oldEvent->url)->first() : null;
                $oldVenue = DB::connection('legacy')->table('places')->where('id', $reservation->place_id)->first();
                $venue = $oldVenue ? Venue::where('slug', $oldVenue->url)->first() : null;
                $oldUser = DB::connection('legacy')->table('user')->where('id', $reservation->user_id)->first();
                $user = $oldUser ? User::where('slug', $oldUser->url)->first() : null;
                $oldTeam= DB::connection('legacy')->table('team')->where('id', $reservation->team_id)->first();
                $team = $oldTeam ? Team::where('slug', $oldTeam->url)->first() : null;

                Reservation::create([
                    'event_id' => $event ? $event->id : null,
                    'venue_id' => $venue ? $venue->id : null,
                    'user_id' => $user ? $user->id : null,
                    'team_id' => $team ? $team->id : null,
                    'name' => $reservation->name,
                    'phone' => $reservation->phone,
                    'email' => $reservation->email,
                    'tables' => 1,
                    'note' => $reservation->note,
                    'fix' => $reservation->fix_reservation,
                    'edit_hash' => $reservation->edit_hash
                ]);

                $bar->advance();
            });
            $bar->finish();
            $this->info(PHP_EOL.'Done.');
        });
    }
}
