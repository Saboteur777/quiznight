<?php

namespace App\Console\Commands;

use App\Models\Event;
use App\Models\Score;
use App\Models\Team;
use App\Models\User;
use App\Models\Venue;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateScores extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:scores';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function() {
            $this->info('Migrating scores...');
            $scores = DB::connection('legacy')->table('score')->get();
            $bar = $this->output->createProgressBar(count($scores));

            $scores->each(function($score) use ($bar){
                $oldEvent = DB::connection('legacy')->table('event')->where('id', $score->event_id)->first();
                $newEvent = Event::where('slug', $oldEvent->url)->first();
                $oldTeam = DB::connection('legacy')->table('team')->where('id', $score->team_id)->first();
                $newTeam = Team::where('slug', $oldTeam->url)->first();
                $oldVenue = DB::connection('legacy')->table('places')->where('id', $score->place_id)->first();
                $newVenue = Venue::where('slug', $oldVenue->url)->first();
                $oldAdmin = DB::connection('legacy')->table('user')->where('id', $score->admin_id)->first();
                $newAdmin = User::where('email', $oldAdmin->email)->first();

                Score::create([
                    'event_id' => $newEvent->id,
                    'team_id' => $newTeam->id,
                    'venue_id' => $newVenue->id,
                    'admin_id' => $newAdmin->id,
                    'score' => $score->score
                ]);

                $bar->advance();
            });

            $bar->finish();
            $this->info(PHP_EOL.'Done.');
        });
    }
}
