<?php

namespace App\Console\Commands;

use App\Models\Page;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigratePages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:pages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function() {
            $this->info('Migrating pages...');
            $pages = DB::connection('legacy')->table('static')->get();
            $bar = $this->output->createProgressBar(count($pages));

            $pages->each(function($page) use ($bar) {
                Page::create([
                    'excerpt' => $page->description,
                    'title' => $page->title,
                    'slug' => $page->name,
                    'content' => $page->content,
                    'created_at' => $page->created_at
                ]);
                $bar->advance();
            });

            $bar->finish();
            $this->info(PHP_EOL.'Done.');
        });
    }
}
