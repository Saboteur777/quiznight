<?php

namespace App\Console\Commands;

use App\Models\Badge;
use App\Models\BadgeCategory;
use App\Models\Team;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateBadges extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:badges';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function() {
            $this->info('Migrationg badge categories...');
            $categories = DB::connection('legacy')->table('badge_category')->get();
            $bar = $this->output->createProgressBar(count($categories));

            $categories->each(function($category) use ($bar) {
                BadgeCategory::create([
                    'name' => $category->name,
                    'display_name' => $category->title,
                    'description' => $category->description
                ]);
                $bar->advance();
            });
            $bar->finish();
            $this->info('Done.');

            $this->info('Migrationg badges...');
            $badges = DB::connection('legacy')->table('badge')->get();
            $bar = $this->output->createProgressBar(count($badges));

            $badges->each(function($badge) use ($bar) {
                $oldCategory = DB::connection('legacy')->table('badge_category')->where('id', $badge->category_id)->first();
                $newCategory = BadgeCategory::where('name', $oldCategory->name)->first();

                Badge::create([
                   'badge_category_id' => $newCategory->id,
                   'name' => $badge->name,
                   'value' => $badge->value,
                   'image' => $badge->image,
                   'description' => $badge->description,
                   'is_active' => $badge->is_active,
                   'created_at' => $badge->created_at
                ]);
                $bar->advance();
            });
            $bar->finish();
            $this->info('Done.');

            $this->info('Migrationg badge relationships...');
            $rel = DB::connection('legacy')->table('team_badge')->get();
            $bar = $this->output->createProgressBar(count($rel));

            $rel->each(function($rel) use ($bar){
                $oldBadge = DB::connection('legacy')->table('badge')->where('id', $rel->badge_id)->first();
                $newBadge = Badge::where('name', $oldBadge->name)->first();
                $oldTeam = DB::connection('legacy')->table('team')->where('id', $rel->team_id)->first();
                $newTeam = Team::where('slug', $oldTeam->url)->first();

                DB::table('badge_team')->insert([
                    'badge_id' => $newBadge->id,
                    'team_id' => $newTeam->id
                ]);
                $bar->advance();
            });
            $bar->finish();
            $this->info('Done.');
        });
    }
}
