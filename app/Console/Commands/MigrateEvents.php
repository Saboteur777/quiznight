<?php

namespace App\Console\Commands;

use App\Models\Championship;
use App\Models\Event;
use App\Models\EventResult;
use App\Models\Team;
use App\Models\Venue;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:events';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Migrating events...');

        DB::transaction(function() {
            $events = DB::connection('legacy')->table('event')->get();
            $bar = $this->output->createProgressBar(count($events));

            $events->each(function($event) use ($bar) {
                $oldChampionship = DB::connection('legacy')->table('championship')->where('id', $event->championship_id)->first();
                $newChampionship = Championship::where('slug', $oldChampionship->name)->first();

                Event::create([
                    'championship_id' => $newChampionship->id,
                    'name' => $event->name,
                    'slug' => $event->url,
                    'date' => $event->date == '0000-00-00 00:00:00' ? null : $event->date,
                    'championship' => $event->championship,
                    'is_active' => $event->is_active,
                    'is_private' => $event->is_private,
                    'reservation_starts' => $event->reservation_starts == '0000-00-00 00:00:00' ? null : $event->reservation_starts,
                    'reservation_modify_ends' => $event->modify_ends == '0000-00-00 00:00:00' ? null : $event->modify_ends
                ]);
                $bar->advance();
            });
            $bar->finish();

            $eventDetails = DB::connection('legacy')->table('place_event')->get();
            $bar = $this->output->createProgressBar(count($eventDetails));

            $eventDetails->each(function($detail) use($bar) {
                $oldWinner = DB::connection('legacy')->table('team')->where('id', $detail->winner_id)->first();
                $oldLightningWinner = DB::connection('legacy')->table('team')->where('id', $detail->lightning_winner_id)->first();
                $oldEvent = DB::connection('legacy')->table('event')->where('id', $detail->event_id)->first();
                $oldVenue = DB::connection('legacy')->table('places')->where('id', $detail->place_id)->first();

                $newWinner = $oldWinner ? Team::where('slug', $oldWinner->url)->first() : 0;
                $newLightningWinner = $oldLightningWinner ? Team::where('slug', $oldLightningWinner->url)->first() : 0;
                $newEvent = Event::where('slug', $oldEvent->url)->first();
                $newVenue = Venue::where('slug', $oldVenue->url)->first();

                EventResult::create([
                    'event_id' => $newEvent->id,
                    'venue_id' => $newVenue->id,
                    'winner_id' => $newWinner ? $newWinner->id : null,
                    'lightning_winner_id' => $newLightningWinner ? $newLightningWinner->id : null
                ]);
                $bar->advance();
            });

            $bar->finish();
        });

        $this->info('Done.');
    }
}
