<?php

namespace App\Console\Commands;

use App\Models\Question;
use App\Models\User;
use DB;
use Illuminate\Console\Command;

class MigrateQuestions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:questions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function() {
            $this->info('Migrating questions...');

            $questions = DB::connection('legacy')->table('kerdesek')->get();
            $bar = $this->output->createProgressBar(count($questions));

            $questions->each(function($question) use ($bar) {
                $oldUser = DB::connection('legacy')->table('user')->where('id', $question->user_id)->first();
                $newUser = $oldUser ? User::where('email', $oldUser->email)->first() : null;

                Question::create([
                    'user_id' => $newUser ? $newUser->id : null,
                    'question' => $question->question,
                    'answer' => $question->answer,
                    'accepted' => $question->is_active,
                    'category' => $question->category,
                    'source' => $question->source,
                    'created_at' => $question->created_at
                ]);

                $bar->advance();
            });
            $bar->finish();

            $this->info('Done.');
        });
    }
}
