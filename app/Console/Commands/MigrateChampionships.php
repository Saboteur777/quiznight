<?php

namespace App\Console\Commands;

use App\Models\Championship;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateChampionships extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:championships';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Migrating championships...');

        $championships = DB::connection('legacy')->table('championship')->get();
        $bar = $this->output->createProgressBar(count($championships));

        $championships->each(function($championship) use($bar) {
            Championship::create([
                'title' => $championship->title,
                'slug' => $championship->name,
                'description' => $championship->description,
                'season' => $championship->year
            ]);

            $bar->advance();
        });

        $bar->finish();
        $this->info('Done.');
    }
}
