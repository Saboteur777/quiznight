<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Venue;
use App\Models\VenueTag;
use DB;
use Illuminate\Console\Command;

class MigrateVenues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:venues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Migrating venues...');

        $venues = DB::connection('legacy')->table('places')->get();
        $bar = $this->output->createProgressBar(count($venues));

        $venues->each(function ($venue) use($bar) {
            $oldAdmin = DB::connection('legacy')->table('user')->where('id', $venue->admin_id)->first();
            $newAdmin = User::where('email', $oldAdmin->email)->first()
            ;
            $newVenue = Venue::create([
                'slug' => $venue->url,
                'name' => $venue->name,
                'language' => $venue->lang,
                'admin_id' => $newAdmin->id,
                'city' => $venue->city,
                'country' => $venue->country,
                'postal_code' => $venue->postal_code,
                'address' => $venue->address,
                'tables' => round($venue->space/5),
                'website' => $venue->web,
                'email' => $venue->email,
                'phone' => $venue->phone,
                'image' => $venue->image,
                'description' => $venue->description,
                'geolocation' => $venue->map,
                'secret' => $venue->hash,
                'is_active' => $venue->is_active,
                'created_at' => $venue->created_at
            ]);

            $bar->advance();
        });
        $bar->finish();
        $this->info(PHP_EOL.'Done.');

        $tags = DB::connection('legacy')->table('pictogram')->get();
        $bar = $this->output->createProgressBar(count($tags));

        $tags->each(function($tag) use ($bar) {
            VenueTag::create([
                'title' => $tag->title,
                'is_active' => $tag->is_active
            ]);
            $bar->advance();
        });
        $bar->finish();
        $this->info(PHP_EOL.'Done.');

        $rel = DB::connection('legacy')->table('place_pictogram')->get();
        $bar = $this->output->createProgressBar(count($rel));

        $rel->each(function($rel) use ($bar) {
            $oldVenue = DB::connection('legacy')->table('places')->where('id', $rel->place_id)->first();
            $newVenue = Venue::where('slug', $oldVenue->url)->first();
            $oldTag = DB::connection('legacy')->table('pictogram')->where('id', $rel->pictogram_id)->first();
            $newTag = VenueTag::where('title', $oldTag->title)->first();

            DB::table('venue_venue_tag')->insert([
                'venue_id' => $newVenue->id,
                'venue_tag_id' => $newTag->id
            ]);

            $bar->advance();
        });
        $bar->finish();

        $this->info(PHP_EOL.'Done.');
    }
}
