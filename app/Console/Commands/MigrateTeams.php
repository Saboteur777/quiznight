<?php

namespace App\Console\Commands;

use App\Models\Team;
use App\Models\TeamMemberShip;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateTeams extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:teams';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Migrating teams...');
        DB::transaction(function () {
            $teams = DB::connection('legacy')->table('team')->get();
            $bar = $this->output->createProgressBar(count($teams));

            $teams->each(function ($team) use ($bar) {
                $newTeam = Team::create([
                    'slug' => $team->url,
                    'name' => $team->name,
                    'description' => $team->description,
                    'image' => $team->image,
                    'reservation_default_note' => $team->reservation_default_note,
                    'is_active' => $team->is_active,
                    'created_at' => $team->created_at
                    // TODO fix reservations
                ]);

                DB::connection('legacy')->table('user_team')->where('team_id', $team->id)->groupBy('user_team.user_id')->get()->each(function($rel) use ($team, $newTeam) {
                    $oldUser = DB::connection('legacy')->table('user')->where('id', $rel->user_id)->first();
                    $newUser = User::where('email', $oldUser->email)->first();

                    DB::table('team_user')->insert([
                        'user_id' => $newUser->id,
                        'team_id' => $newTeam->id,
                        'is_active' => $rel->is_active,
                        'is_admin' => $rel->user_id == $team->admin_id ? true : false,
                        'created_at' => $rel->created_at
                    ]);
                });

                $bar->advance();
            });
            $bar->finish();
        });

        $this->info('Done.');
    }
}
