<?php

namespace App\Console\Commands;

use App\Models\Post;
use App\Models\User;
use App\Models\Venue;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigratePosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function() {
            $this->info('Migrating posts...');
            $posts = DB::connection('legacy')->table('news')->get();
            $bar = $this->output->createProgressBar(count($posts));

            $posts->each(function($post) use ($bar) {
                $oldAuthor = DB::connection('legacy')->table('user')->where('id', $post->author)->first();
                $author = $oldAuthor ? User::where('slug', $oldAuthor->url)->first() : null;
                $oldVenue = DB::connection('legacy')->table('places')->where('id', $post->place_id)->first();
                $venue = $oldVenue ? Venue::where('slug', $oldVenue->url)->first() : null;

                Post::create([
                    'title' => $post->title,
                    'slug' => $post->name,
                    'author_id' => $author ? $author->id : null,
                    'excerpt' => $post->description,
                    'image' => $post->image,
                    'content' => $post->content,
                    'venue_id' => $venue ? $venue->id : null,
                    'gallery_url' => $post->gallery,
                    'published' => $post->is_published,
                    'draft' => $post->is_draft,
                    'created_at' => $post->created_at,
                    'updated_at' => $post->modified_at
                ]);

                $bar->advance();
            });
            $bar->finish();
            $this->info(PHP_EOL.'Done.');
        });
    }
}
