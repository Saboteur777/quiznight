<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MigrateLegacy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:legacy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate data from legacy database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('migrate:users');
        $this->call('migrate:teams');
        $this->call('migrate:venues');
        $this->call('migrate:championships');
        $this->call('migrate:events');
        $this->call('migrate:badges');
        $this->call('migrate:questions');
        $this->call('migrate:scores');
        $this->call('migrate:reservations');
        $this->call('migrate:posts');
        $this->call('migrate:pages');
    }
}
