<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate users from legacy database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Migrating users...');

        $users = DB::connection('legacy')->table('user')->get();
        $bar = $this->output->createProgressBar(count($users));

        $users->each(function ($user) use ($bar) {
            $newUser = User::create([
                'slug' => $user->url,
                'username' => $user->nick,
                'email' => $user->email,
                'password' => $user->password,
                'name' => $user->name,
                'gender' => $user->gender == 'no' ? 'female' : 'male',
                'birth_date' => $user->birth_date == '0000-00-00' ? null :  Carbon::parse($user->birth_date),
                'image' => $user->image,
                'description' => $user->description,
                'phone' => $user->phone,
                'is_active' => $user->is_active,
                'created_at' => $user->created_at
            ]);

            $bar->advance();
        });

        $bar->finish();
        $this->info('Done.');
    }
}
