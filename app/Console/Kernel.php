<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\MigrateLegacy::class,
        Commands\MigrateUsers::class,
        Commands\MigrateTeams::class,
        Commands\MigrateVenues::class,
        Commands\MigrateChampionships::class,
        Commands\MigrateEvents::class,
        Commands\MigrateBadges::class,
        Commands\MigrateQuestions::class,
        Commands\MigrateScores::class,
        Commands\MigrateReservations::class,
        Commands\MigratePosts::class,
        Commands\MigratePages::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
