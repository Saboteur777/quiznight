<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Traits\FlashNotify;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    use FlashNotify;

    public function index(){
        $users = User::paginate(100);

        return view('admin.users.index', compact('users'));
    }

    public function search(Request $request){
        $users = User::where('email', 'like', '%'.$request->q.'%')
                        ->orWhere('username', 'like', '%'.$request->q.'%')
                        ->orWhere('name', 'like', '%'.$request->q.'%')
                        ->paginate(100);

        return view('admin.users.index', compact('users'));
    }

    public function get(User $user){
        return view('admin.users.user', compact('user'));
    }

    public function update(Request $request, User $user){
        $v = Validator::make($request->all(), [
            'name' => 'required|min:2',
            'username' => 'required|min:1',
            'email' => 'required|email'
        ]);

        $v->sometimes('email', 'unique:users', function ($input) use ($user) {
           return $input->email != $user->email;
        });

        $v->sometimes('username', 'unique:users', function ($input) use ($user) {
            return $input->username != $user->username;
        });

        if($v->fails()) {
            return back()->withInput($request->all())->withErrors($v);
        }

        $user->update($request->only(['name', 'phone', 'username', 'email', 'gender', 'description']));

        $this->flashSuccess('A felhasználó adatait sikeresen mentettük!');

        return back();
    }
}
