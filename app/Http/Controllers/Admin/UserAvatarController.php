<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Traits\FlashNotify;
use App\Models\User;
use App\Models\UserAvatar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class UserAvatarController extends Controller
{
    use FlashNotify;

    protected $extension = '.jpg';

    public function store(Request $request, User $user){
        $v = Validator::make($request->all(), [
            'image' => 'required|image|max:5000|dimensions:min_width=150,min_height=150'
        ]);

        if($v->fails()) {
            return back()->withInput($request->all())->withErrors($v);
        }

        $filename = md5($request->file('image') . microtime());

        $path = $request->file('image')->storePubliclyAs('public/users', $filename.$this->extension);

        collect(config('avatars'))->each(function($type) use ($request, $filename) {
            $img = Image::make($request->file('image'));
            if($type['fixed_ratio']) {
                $img->fit($type['width'], $type['height']);
            }
            else {
                if($img->width() >= $img->height()) {
                    $img->resize($type['width'], null, function ($constraint) {
                       $constraint->aspectRatio();
                    });
                }
                else {
                    $img->resize($type['height'], null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
            }
            $img->stream('jpg');
            Storage::put('public/users/'.$type['prefix'].$filename.$this->extension, $img);
        });

        $avatar = new UserAvatar(['filename' => $filename.$this->extension]);
        $user->avatar()->save($avatar);

        $this->flashSuccess('Sikeresen mentettük a profilképet!');

        return back();
    }
}
