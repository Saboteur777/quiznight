<?php

namespace App\Http\Controllers\Admin;

use App\Models\Badge;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BadgeController extends Controller
{
    public function index(){
        $badges = Badge::active()->paginate(100);

        return view('admin.badges.index', compact('badges'));
    }
}
