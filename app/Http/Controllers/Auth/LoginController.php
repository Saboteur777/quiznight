<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('app.auth.login');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        // Handle legacy passwords
        // Legacy passwords are bcrypted md5 passwords and start with '$md5'
        if ($user && substr($user->password, 0, 4) == '$md5') {
            // Cut the '$md5' substring from password to get hash
            $hash = substr($user->password, 4);
            $password = md5($request->password);
            // If password is correct, rehash the password with bcrypt and save user
            if (password_verify($password, $hash)) {
                $user->password = bcrypt($request->password);
                $user->save();
            }
        }

        return $this->guard()->attempt(
            $this->credentials($request), $request->has('remember')
        );
    }
}
