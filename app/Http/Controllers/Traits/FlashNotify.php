<?php

namespace App\Http\Controllers\Traits;

trait FlashNotify
{
    protected function flashStatus($type, $message)
    {
        session()->flash('status', ['type' => $type, 'message' => $message]);
    }

    protected function flashSuccess($message)
    {
        $this->flashStatus('success', $message);
    }

    protected function flashInfo($message)
    {
        $this->flashStatus('info', $message);
    }

    protected function flashWarning($message)
    {
        $this->flashStatus('warning', $message);
    }

    protected function flashError($message)
    {
        $this->flashStatus('danger', $message);
    }
}