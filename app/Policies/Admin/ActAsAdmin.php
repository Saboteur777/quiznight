<?php

namespace App\Policies\Admin;

use App\Models\User;

trait ActAsAdmin
{
    /**
     * Check permission for given name
     *
     * @param User $user
     * @return mixed
     */
    public function handle(User $user){
        return $user->permissions->contains('name', $this->permissionName);
    }

    /**
     * Handle superadmin rights
     *
     * @param $user
     * @return boolean
     */
    public function before($user){
        return $user->permissions->contains('name', 'superadmin');
    }
}