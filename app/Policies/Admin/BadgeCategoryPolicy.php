<?php

namespace App\Policies\Admin;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BadgeCategoryPolicy
{
    use HandlesAuthorization, ActAsAdmin;

    protected $permissionName = 'badge_categories';
}
