<?php

namespace App\Policies\Admin;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OfferRequestPolicy
{
    use HandlesAuthorization, ActAsAdmin;

    protected $permissionName = 'offer_requests';
}
