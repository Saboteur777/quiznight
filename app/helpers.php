<?php

function create_slug($title){

    $url_eredmeny = "";

    $mirol = array( "í", "é", "á", "ű", "ő", "ú", "ö", "ü", "ó", "Í", "É", "Á", "Ű", "Ő", "Ú", "Ö", "Ü", "Ó", " ", "-", "/", "<", ">", "#", "&", "@", "{", "}", "[", "]", "$", "\"", "'", "\\", ";", "*", "+" );
    $mire =  array( "i", "e", "a", "u", "o", "u", "o", "u", "o", "I", "E", "A", "U", "O", "U", "O", "U", "O", "_", "_", "", "",  "",  "",  "",  "",  "",  "",  "",  "",  "",  "",   "",   "",   "",  "",  "" );
    $name_uj = str_replace( $mirol, $mire, $title); // ékezeteseket változtat, szóköz helyett _ -t rak
    $name_uj = strtolower($name_uj);  // minden kisbetű
    //csak a megfelelő karakterket hagyja az url-ben {a-z kisbetűk és alulvonás}
    $minta="/([a-z0-9]|_)*/";
    preg_match_all($minta,$name_uj,$egyezes,PREG_SET_ORDER);
    foreach ($egyezes as $egy){
        $url_eredmeny.=$egy[0];
    }
    $url_eredmeny=preg_replace("/_{2,}/","_",$url_eredmeny);
    $url_eredmeny=preg_replace("/^_/","",$url_eredmeny);
    $url_eredmeny=preg_replace("/_$/","",$url_eredmeny);

    return $url_eredmeny;
}