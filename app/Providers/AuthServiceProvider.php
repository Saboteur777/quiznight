<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Models\User' => 'App\Policies\Admin\UserPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        // Register admin policies
        $this->addAdminPolicies();

        $this->registerPolicies();

        //
    }

    /**
     * Add admin policies from config file
     */
    protected function addAdminPolicies()
    {
        array_map(function($permission) {
            if (array_key_exists('model', $permission)) {
                $this->policies['App\\Models\\'.$permission['model']] ='App\\Policies\\Admin\\'.$permission['model'].'Policy';
            }
        }, config('permissions'));
    }
}
