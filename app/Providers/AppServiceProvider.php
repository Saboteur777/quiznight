<?php

namespace App\Providers;

use App\Models\Team;
use App\Models\User;
use App\Models\Venue;
use App\Observers\TeamObserver;
use App\Observers\UserObserver;
use App\Observers\VenueObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register IDE helper if not in production
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
